import { QoreClient } from '@feedloop/qore-client';
import config from '../qore.config.json';
import schema from '../qore.schema.json';

const client = new QoreClient({
  ...config,
  getToken: () => window.localStorage.getItem('token') || '',
  onError: (error: any) => {
    const { response: { status } } = error;
    if (status === 401) {
      window.location.href = '/login';
    }
  },
});
client.init(schema as any);

export default client;
